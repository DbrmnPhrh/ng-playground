import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';
import {autoUnsubscribe} from '../main/shared/utils/auto-unsubscribe';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidebarComponent implements OnInit {

  private resizeSubscription: Subscription;

  menuItems: IMenuItem[] = [
    {name: 'Angular features', isHeader: true},
    {name: 'template-test', url: 'angular-features/template-test'},
    {name: 'ng-content', url: 'angular-features/ng-content'},
    {name: 'content-child', url: 'angular-features/content-child'},
    {name: 'user-by-id', url: 'angular-features/user-by-id/1'},
    {name: 'users-from-resolver', url: 'angular-features/users-from-resolver'},
    {name: 'directives-test', url: 'angular-features/directives-test'},
    {name: 'view-child', url: 'angular-features/view-child'},
    {name: 'token-test', url: 'angular-features/token-test'},
    {name: 'output-input', url: 'angular-features/output-input'},
    {name: 'dynamic-todos', url: 'angular-features/dynamic-todos'},

    {name: 'RxJS features', isHeader: true},
    {name: 'RxJS', url: 'rxjs/rxjs'},

    {name: 'Typescript features', isHeader: true},
    {name: 'Type features', url: 'typescript/type-features'},

    {name: 'Other', isHeader: true},
    {name: 'custom-checkbox', url: 'other/custom-checkbox'},
    {name: 'custom-tooltip', url: 'other/custom-tooltip'},
    {name: 'multi-level-menu', url: 'other/multi-level-menu'},
    {name: 'switcher', url: 'other/switcher'},
    {name: 'not-found', url: '/not-found'},
    {name: 'responsive-table', url: 'other/responsive-table'},
    {name: 'style-features', url: 'other/style-features'},
    {name: 'ng-select', url: 'other/ng-select'},
  ];

  @ViewChild('sidebar', {static: true}) sidebarElem;
  @ViewChild('resize', {static: true}) resizeElem;
  @HostBinding('style.width.px') width = 350;
  minWidth = 200;
  maxWidth = 1200;

  constructor(
    private cdr: ChangeDetectorRef,
    private currentComponent: ViewContainerRef,
  ) { }

  ngOnInit(): void {
    this.sidebarElem.nativeElement.style.width = `${this.width}px`;
    this.resizeElem.nativeElement.style.left = `${this.width}px`;
    this.resizeElem.nativeElement.onselectstart = () => false;
    this.currentComponent.element.nativeElement.onselectstart = () => false;
  }

  doResize(event: MouseEvent): void {
    event.stopPropagation();

    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }

    this.resizeSubscription = fromEvent(window, 'mousemove').subscribe((e: MouseEvent) => {
      const newWidth = e.clientX;

      if (newWidth > this.minWidth && newWidth < this.maxWidth) {
        this.width = newWidth;
        this.sidebarElem.nativeElement.style.width = `${this.width}px`;
        this.resizeElem.nativeElement.style.left = `${this.width}px`;
      }
      this.cdr.markForCheck();
    });

    this.resizeSubscription.add(fromEvent(window, 'mouseup').subscribe(() => {
      if (this.resizeSubscription) {
        this.resizeSubscription.unsubscribe();
      }
    }));
  }

}

export interface IMenuItem {
  name: string;
  url?: string;
  isHeader?: boolean;
}
