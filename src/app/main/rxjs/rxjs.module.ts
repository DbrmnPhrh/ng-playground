import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RxjsTestComponent} from './rxjs-test/rxjs-test.component';
import {SharedModule} from '../shared/shared.module';

const routes: Routes = [
  {path: '', pathMatch: 'prefix', redirectTo: 'rxjs'},
  {path: 'rxjs', component: RxjsTestComponent},
];

@NgModule({
  declarations: [
    RxjsTestComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule, // todo нужно ?
  ]
})
export class RxjsModule { }
