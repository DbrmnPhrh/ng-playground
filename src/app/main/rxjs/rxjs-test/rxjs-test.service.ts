import { Injectable } from '@angular/core';
import {BehaviorSubject, ReplaySubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RxjsTestService {

  public subject$: Subject<string> = new Subject<string>();
  public subjectReplay$: ReplaySubject<string> = new ReplaySubject<string>(1);
  public subjectBehavior$: BehaviorSubject<string> = new BehaviorSubject<string>('some value 1');

  constructor() {
    this.subjectReplay$.next('some value');
    this.subjectBehavior$.next('some value 2');
  }
}
