import {ChangeDetectionStrategy, Component, HostBinding, OnInit, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject, interval, ReplaySubject, Subject, timer} from 'rxjs';
import {scan, shareReplay, take, takeWhile, tap} from 'rxjs/operators';
import {RxjsTestService} from './rxjs-test.service';
import {autoUnsubscribe} from '../../shared/utils/auto-unsubscribe';

@Component({
  selector: 'app-rxjs-test',
  template: `
    <h3>{{title}}</h3>
    <div>
      {{source$ | async}}
      {{source$ | async}}

      <button (click)="addSubscribers()">addSubscribers()</button>
    </div>
    <br>
    <h3>Countdown timer:</h3>
    <h3>{{ timer$ | async | time }}</h3>
    <br>
    <h3>Subject: {{subject$ | async}}</h3>
    <h3>ReplaySubject: {{subjectReplay$ | async}}</h3>
    <h3>BehaviorSubject: {{subjectBehavior$ | async}}</h3>
  `,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RxjsTestComponent implements OnInit {

  @HostBinding('class.commonHeader')
  commonHeader = true;

  title = 'publish() + refCount(), share()';
  source$;
  timer$ = timer(0, 1000).pipe(
    scan(acc => --acc, 120),
    takeWhile(x => x >= 0),
  );
  subject$: Subject<string> = this.rxjsTestService.subject$;
  subjectReplay$: ReplaySubject<string> = this.rxjsTestService.subjectReplay$;
  subjectBehavior$: BehaviorSubject<string> = this.rxjsTestService.subjectBehavior$;

  constructor(private rxjsTestService: RxjsTestService) {
  }

  ngOnInit(): void {
    setTimeout(() => this.rxjsTestService.subject$.next('some value'));
    this.source$ = interval(590).pipe(
      take(5),
      tap(t => console.log(t)),
      // publish(),
      // refCount(),
      shareReplay({refCount: true, bufferSize: 1}),
      // share(),
    );
  }

  addSubscribers(): void {
    this.source$.pipe(autoUnsubscribe()).subscribe(data => console.log('lateSubscriber1', data));

    setTimeout(() => {
      this.source$.pipe(autoUnsubscribe()).subscribe(data => console.log('lateSubscriber2', data));
    }, 1000);
  }
}
