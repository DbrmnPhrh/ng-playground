import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IUser} from '../user-by-id/user-by-id.service';

@Component({
  selector: 'app-users-from-resolver',
  templateUrl: './users-from-resolver.component.html',
  styleUrls: ['./users-from-resolver.component.sass'],
})
export class UsersFromResolver implements OnInit {

  users$: Observable<IUser[]> = this.route.data.pipe(
    map((users: Data) => users.usersData),
  );
  userId = 0;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
  }
}
