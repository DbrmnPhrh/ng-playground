import {inject, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {
  ViewChildParentComponent,
  ViewChildOneComponent,
  ViewChildTwoComponent,
  ViewChildThreeComponent,
  ViewChildFourComponent,
  ViewChildFiveComponent,
  ViewChildSixComponent,
  ViewChildSevenComponent,
} from './view-child/view-child-parent.component';
import {DirectivesTestComponent} from './directives-test/directives-test.component';
import {SharedModule} from '../shared/shared.module';
import {NgContentComponent} from './ng-content/ng-content.component';
import {TemplateTestComponent} from './template-test/template-test.component';
import {Test1Guard} from '../shared/guards/test1-guard.service';
import {UserByIdComponent} from './user-by-id/user-by-id.component';
import {UsersFromResolver} from './user-by-id-resolver/users-from-resolver.component';
import {TokensTestComponent} from './tokens-test/tokens-test.component';
import {OutputInputComponent} from './output-input/output-input.component';
import {OutputInputChild1Component} from './output-input/output-input-child1/output-input-child1.component';
import {OutputInputChild2Component} from './output-input/output-input-child2/output-input-child2.component';
import {DynamicTodosComponent} from './dynamic-todos/dynamic-todos.component';
import {TodoItemComponent} from './dynamic-todos/todo-item/todo-item.component';
import {ContentChildComponent} from './content-child/content-child.component';
import {ContentChildChildComponent} from './content-child/content-child-parent/content-child-child/content-child-child.component';
import {ContentChildParentComponent} from './content-child/content-child-parent/content-child-parent.component';
import {UserByIdService} from './user-by-id/user-by-id.service';

const routes: Routes = [
  {path: '', pathMatch: 'prefix', redirectTo: 'template-test'},
  {
    path: 'template-test',
    component: TemplateTestComponent,
    canActivate: [Test1Guard],
    canDeactivate: [Test1Guard],
    data: {
      someName: 'Template test component...',
      someData: [1, 2, 3, 4, 5],
    }
  },
  {path: 'ng-content', component: NgContentComponent},
  {path: 'content-child', component: ContentChildComponent},
  {path: 'user-by-id/:id', component: UserByIdComponent},
  {path: 'users-from-resolver',
    component: UsersFromResolver,
    resolve: {
      usersData: () => inject(UserByIdService).getAllUsersData(),
    },
  },
  {path: 'directives-test', component: DirectivesTestComponent},
  {path: 'view-child', component: ViewChildParentComponent},
  {path: 'token-test', component: TokensTestComponent},
  {path: 'output-input', component: OutputInputComponent},
  {path: 'dynamic-todos', component: DynamicTodosComponent}, // при изменении url также исправить в main.component, иначе поедет вёрстка!
];


@NgModule({
  declarations: [
    DirectivesTestComponent,
    ViewChildParentComponent,
    ViewChildOneComponent,
    ViewChildTwoComponent,
    ViewChildThreeComponent,
    ViewChildFourComponent,
    ViewChildFiveComponent,
    ViewChildSixComponent,
    ViewChildSevenComponent,
    TokensTestComponent,
    OutputInputComponent,
    OutputInputChild1Component,
    OutputInputChild2Component,
    DynamicTodosComponent,
    TodoItemComponent,
    ContentChildComponent,
    ContentChildChildComponent,
    ContentChildParentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
  ]
})
export class AngularFeaturesModule { }
