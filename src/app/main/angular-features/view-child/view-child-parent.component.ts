import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {DEFAULT_SETTINGS, IDefaultSettings} from '../../../app.module';

/** Дочерний компонент 1 */
@Component({
  selector: 'app-child-1',
  template: `<p>{{someProp}}</p>`,
})
export class ViewChildOneComponent {
  public someProp = 'hello from child 1!';
}

/** Дочерний компонент 2 */
@Component({
  selector: 'app-child-2',
  template: `<p>{{someProp}}</p>`,
})
export class ViewChildTwoComponent {
  public someProp = 'hello from child 2!';
}

/** Дочерний компонент 3 */
@Component({
  selector: 'app-child-3',
  template: `<p>{{someProp}}</p>`,
})
export class ViewChildThreeComponent {
  @Input() someProp: string;
}

/** Дочерний компонент 4 */
@Component({ selector: 'app-child-4', template: `<p>{{someProp}}</p>` })
export class ViewChildFourComponent { public someProp: 'child-4'; }

/** Дочерний компонент 5 */
@Component({ selector: 'app-child-5', template: `<p>{{someProp}}</p>` })
export class ViewChildFiveComponent { public someProp: 'child-5'; }

/** Дочерний компонент 6 */
@Component({ selector: 'app-child-6', template: `<p>{{someProp}}</p>` })
export class ViewChildSixComponent { public someProp: 'child-6'; }

/** Дочерний компонент 7 */
@Component({ selector: 'app-child-7', template: `<p>{{someProp}}</p>` })
export class ViewChildSevenComponent { public someProp: 'child-7'; }

/** Родительский компонент */
@Component({
  selector: 'app-parent',
  template: `
    <p>parent component:</p>
    <p class="ms-3">- child 1 prop: {{this.viewChild1.someProp}}</p>
    <p class="ms-3">- child 2 prop: {{this.viewChild2.someProp}}</p>
    <p class="ms-3">- child 3 prop: {{this.viewChild3.someProp}}
      <span style="font-size: 12px; color: red">Не работает без ChangeDetectionStrategy.OnPush!</span>
    </p>
    <app-child-1></app-child-1>
    <app-child-2 #child2></app-child-2>
    <app-child-3 #child3 [someProp]="'child3: message from parent'"></app-child-3>
    <app-child-4 #child></app-child-4>
    <app-child-5 #child></app-child-5>
    <app-child-6 #child></app-child-6>
    <app-child-7></app-child-7>
    <p>{{'defaultSettings.someProperty: ' + defaultSettings.someProperty}}</p>
    <p>
      {{'defaultSettings.someText: ' + defaultSettings.someText}}
      <span style="font-size: 12px; color: red">При нажатии "Change someText" в разделе token-test строка поменяется!</span>
    </p>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewChildParentComponent implements OnInit, AfterViewInit  {

  @ViewChild(ViewChildOneComponent, {static: true}) viewChild1: ViewChildOneComponent;
  @ViewChild('child2', {static: true}) viewChild2: ViewChildTwoComponent;
  @ViewChild('child3', {static: true}) viewChild3: ViewChildThreeComponent;
  @ViewChildren('child') viewChildrenChild: QueryList<ViewChildFourComponent | ViewChildFiveComponent | ViewChildSixComponent>;
  @ViewChildren(ViewChildSevenComponent) ViewChild7: QueryList<ViewChildSevenComponent>;

  constructor(
    private cdr: ChangeDetectorRef,
    @Inject(DEFAULT_SETTINGS) public defaultSettings: IDefaultSettings,
  ) {}

  ngOnInit(): void {
    console.log('viewChild1: ', this.viewChild1);
    console.log('viewChild2: ', this.viewChild2);
    console.log('viewChild3: ', this.viewChild3);
  }

  ngAfterViewInit(): void {
    console.log('this.viewChildrenChild: ', this.viewChildrenChild.toArray());
    console.log('this.ViewChild7: ', this.ViewChild7.get(0));
    this.cdr.detectChanges();
  }
}
