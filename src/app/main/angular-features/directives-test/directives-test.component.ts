import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-directives-test',
  templateUrl: './directives-test.component.html',
  styleUrls: ['./directives-test.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DirectivesTestComponent implements OnInit {

  constructor() { }

  public isDirectiveApplied = false;
  public readonly items = [
    {id: 1},
    {id: 2},
    {id: 3},
  ];
  public random = 1;
  public inputTest: number;

  ngOnInit(): void {
  }

  public inputTestChange(): void {
    console.log('this.inputTest: ', this.inputTest);
  }

  applyDirectives(): void {
    this.isDirectiveApplied = !this.isDirectiveApplied;
    this.random = Math.floor(Math.random() * Math.floor(5));
  }
}
