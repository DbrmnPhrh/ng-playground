import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ContentChild,
  AfterContentInit,
  OnChanges,
  ContentChildren,
  QueryList, TemplateRef, AfterContentChecked
} from '@angular/core';
import {ContentChildChildComponent} from './content-child-child/content-child-child.component';

@Component({
  selector: 'app-content-child-parent',
  templateUrl: './content-child-parent.component.html',
  styleUrls: ['./content-child-parent.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentChildParentComponent implements OnInit, AfterContentInit, AfterContentChecked, OnChanges {

  @ContentChild(ContentChildChildComponent, {static: true})
  // static: true означает что компонент статичен и что с ним можно работать до обнаружения изменений
  // static: false означает что компонент динамичен и что с ним можно работать после обнаружения изменений
  appChild?: ContentChildChildComponent; // ! - если уверены, что всегда будет передаваться, ? - если не уверены

  @ContentChildren(ContentChildChildComponent, {read: ContentChildChildComponent})
  appChildren?: QueryList<ContentChildChildComponent>;

  @ContentChild('ref', {static: true})
  appChildByRef?: ContentChildChildComponent;

  @ContentChildren('ref', {read: ContentChildChildComponent})
  appChildrenByRef?: ContentChildChildComponent;

  @ContentChildren('foo,bar,baf,baz', {read: TemplateRef})
  templatesByRef?: QueryList<TemplateRef<{ $implicit: string }>>;

  constructor() { }

  ngOnChanges(): void {
    console.log('ngOnChanges this.appChild: ', this.appChild, this.appChild?.title);
    console.log('ngOnChanges this.appChildren: ', this.appChildren);
    console.log('ngOnChanges this.appChildByRef: ', this.appChildByRef);
    console.log('ngOnChanges this.appChildrenByRef: ', this.appChildrenByRef);
  }

  ngOnInit(): void {
    console.log('ngOnInit this.appChild: ', this.appChild, this.appChild?.title);
    console.log('ngOnInit this.appChildren: ', this.appChildren);
    console.log('ngOnInit this.appChildByRef: ', this.appChildByRef);
    console.log('ngOnInit this.appChildrenByRef: ', this.appChildrenByRef);
  }

  ngAfterContentInit(): void {
    this.appChild?.sayHi();
    if (this.appChild) {
      this.appChild.title = 'title changed';
    }
    console.log('ngAfterContentInit this.appChild: ', this.appChild, this.appChild.title);
    console.log('ngAfterContentInit this.appChildren: ', this.appChildren);
    console.log('ngAfterContentInit this.appChildByRef: ', this.appChildByRef);
    console.log('ngAfterContentInit this.appChildrenByRef: ', this.appChildrenByRef);
    console.log('ngAfterContentInit this.templatesByRef: ', this.templatesByRef);
  }

  ngAfterContentChecked(): void {
    console.log('ngAfterContentInit this.appChild: ', this.appChild, this.appChild.title);
    console.log('ngAfterContentInit this.appChildren: ', this.appChildren);
    console.log('ngAfterContentInit this.appChildByRef: ', this.appChildByRef);
    console.log('ngAfterContentInit this.appChildrenByRef: ', this.appChildrenByRef);
    console.log('ngAfterContentInit this.templatesByRef: ', this.templatesByRef);
  }

}
