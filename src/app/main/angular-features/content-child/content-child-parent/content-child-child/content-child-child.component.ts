import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-content-child-child',
  templateUrl: './content-child-child.component.html',
  styleUrls: ['./content-child-child.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentChildChildComponent implements OnInit {

  message = 'Hi from ContentChildChildComponent';
  title = 'title before init';

  constructor() { }

  ngOnInit(): void {
  }

  sayHi(): void {
    console.log(this.message);
  }

}
