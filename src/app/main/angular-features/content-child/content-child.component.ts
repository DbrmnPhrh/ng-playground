import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-content-child',
  templateUrl: './content-child.component.html',
  styleUrls: ['./content-child.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentChildComponent implements OnInit {

  /** https://www.youtube.com/watch?v=3ACEPgFyn_E&t=329s&ab_channel=МаксимГром */

  title = 'nothing to show';

  constructor() { }

  ngOnInit(): void {
  }

}
