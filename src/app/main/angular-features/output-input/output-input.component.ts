import {Component, ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'app-output-input',
  templateUrl: './output-input.component.html',
  styleUrls: ['./output-input.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputInputComponent {

  messageFromChild1: string;
  messageToChild2: string;
  commonMessageObj: ICommonMessageObj = {} as ICommonMessageObj;
  getMessageFromChild1 = (text: string) => this.messageFromChild1 = text;
  commonMessageObjChange = (newObj) => this.commonMessageObj = newObj;
}

export interface ICommonMessageObj {
  message: string;
}
