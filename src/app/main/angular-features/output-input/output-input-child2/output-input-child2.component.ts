import {Component, ChangeDetectionStrategy, Input} from '@angular/core';
import {ICommonMessageObj} from '../output-input.component';

@Component({
  selector: 'app-oi-child-2',
  templateUrl: './output-input-child2.component.html',
  styleUrls: ['./output-input-child2.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputInputChild2Component {

  @Input() messageFromParent: string;
  @Input() commonMessageObj: ICommonMessageObj;
}
