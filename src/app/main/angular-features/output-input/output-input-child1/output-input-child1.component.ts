import {Component, ChangeDetectionStrategy, Output, EventEmitter, Input} from '@angular/core';
import {interval} from 'rxjs';
import {scan, startWith, take, tap} from 'rxjs/operators';
import {ICommonMessageObj} from '../output-input.component';

@Component({
  selector: 'app-oi-child-1',
  templateUrl: './output-input-child1.component.html',
  styleUrls: ['./output-input-child1.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputInputChild1Component {

  @Output() messageToParentEmitter = new EventEmitter<string>();
  @Output() commonMessageObjChangeEmitter = new EventEmitter<ICommonMessageObj>();
  @Input() commonMessageObj: ICommonMessageObj;

  timer$ = interval(1000).pipe(
    startWith(1),
    scan(acc => --acc, 6),
    take(6),
    tap(currTime => {
      if (currTime === 0) {
        this.messageToParentEmitter.emit('hello from child1!');
      }
    }),
  );

  setMessage2(): void {
    this.messageToParentEmitter.emit('message 2 from child1!');
  }

  setCommonMessageObj(): void {
    this.commonMessageObjChangeEmitter.emit({...this.commonMessageObj});
  }
}
