import { Injectable } from '@angular/core';

/**
 * зарегистрирован в корневом модуле
 * @see AppModule
 */
@Injectable({
  providedIn: 'root'
})
export class TokensTest1Service {

  constructor() { }

  public serviceName = 'tokens test 1 service';
  public randomNumber = Math.floor(Math.random() * Math.floor(5));
}
