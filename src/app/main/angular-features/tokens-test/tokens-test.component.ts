import {Component, ChangeDetectionStrategy, Inject, LOCALE_ID, APP_ID} from '@angular/core';
import {TokensTest2Service} from './tokens-test2.service';
import {TokensTest3Service} from './tokens-test3.service';
import {DEFAULT_SETTINGS, IDefaultSettings} from '../../../app.module';

@Component({
  selector: 'app-tokens-test',
  template: `
    <p><b>LOCALE_ID: </b>{{localeIdValue}}</p>
    <p><b>APP_ID: </b>{{appIdValue}} (меняется после перезапуска приложения)</p>
    <p>{{tokenTest2Service.serviceName + ': ' + tokenTest2Service.randomNumber}}</p>
    <p>{{tokenTest3Service.serviceName + ': ' + tokenTest3Service.randomNumber}}</p>
    <p>{{'defaultSettings.someProperty: ' + somePropertyValue}}</p>
    <div style="display: flex; align-items: baseline;">
      <p>{{'defaultSettings.someText: ' + someTextValue}}</p>
      <button (click)="setSomeText()">Change someText</button>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TokensTestComponent {

  constructor(
    @Inject(LOCALE_ID) private locale: string,
    @Inject(APP_ID) private appId: string,
    public tokenTest2Service: TokensTest2Service,
    public tokenTest3Service: TokensTest3Service,
    @Inject(DEFAULT_SETTINGS) private defaultSettings: IDefaultSettings,
  ) { }

  public get localeIdValue(): string { return this.locale; }
  public get appIdValue(): string { return this.appId; }
  public get somePropertyValue(): boolean { return this.defaultSettings.someProperty; }
  public get someTextValue(): string { return this.defaultSettings.someText; }

  setSomeText(): void {
    this.defaultSettings.someText = 'changed string';
  }

}
