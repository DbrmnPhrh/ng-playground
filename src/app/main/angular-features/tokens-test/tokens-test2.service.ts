import { Injectable } from '@angular/core';

/**
 * зарегистрирован в providers корневого модуля. То же самое что @Injectable({providedIn: 'root'})
 * @see AppModule
 */
@Injectable()
export class TokensTest2Service {

  constructor() { }

  public serviceName = 'tokens test 2 service';
  public randomNumber = Math.floor(Math.random() * Math.floor(5));
}
