import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {IUser, UserByIdService} from './user-by-id.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-user-by-id',
  templateUrl: './user-by-id.component.html',
  styleUrls: ['./user-by-id.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserByIdComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private userService: UserByIdService,
  ) { }

  readonly user$: Observable<IUser> = this.route.params.pipe(
    switchMap((param: IUser): Observable<IUser> => this.userService.getUserData(param.id))
  );

  readonly userIds = [
    {id: 1},
    {id: 2},
    {id: 3},
    {id: 4},
  ];

  ngOnInit(): void {
  }
}
