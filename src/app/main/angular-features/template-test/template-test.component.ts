import {ChangeDetectionStrategy, Component, HostBinding, Input, TemplateRef, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-template-test',
  templateUrl: './template-test.component.html',
  styleUrls: ['./template-test.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class TemplateTestComponent {

  /**
   * У этого компонента и у rxjs-test.component цвет заголовка будет взят из корневого файла стилей styles.sass
   * через свойство commonHeader можно включать/отключать привязку корневого класса
   */
  @HostBinding('class.commonHeader')
  commonHeader = true;


  @Input()
  template?: TemplateRef<{$implicit: number}>;
}
