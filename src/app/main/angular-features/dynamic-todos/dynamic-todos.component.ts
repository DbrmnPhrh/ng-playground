import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  OnDestroy,
} from '@angular/core';
import {TodoItemComponent} from './todo-item/todo-item.component';
import {Subscription} from 'rxjs';

@Component({
  templateUrl: './dynamic-todos.component.html',
  styleUrls: ['./dynamic-todos.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicTodosComponent implements OnInit, OnDestroy {

  @ViewChild('vc', {read: ViewContainerRef, static: true}) vc;
  private componentRefs: ComponentRef<TodoItemComponent>[] = [];
  private subscriptions: Subscription = new Subscription();

  constructor() {}

  ngOnInit(): void {
    this.addComponent();
    this.addComponent();
    this.addComponent();
  }

  addComponent(): void {

    if (this.componentRefs.length === 15) {
      return;
    }

    const componentRef: ComponentRef<TodoItemComponent> = this.vc.createComponent(TodoItemComponent);

    componentRef.instance.data = {
      id: Date.now() - this.getRandomNumber(), // вычитаем рандомное число, т.к. иногда для следующей тудушки берётся предыдущая дата
      headerText: `Текст заголовка`,
      description: 'Текст описания',
    };

    this.subscriptions.add(
      componentRef.instance.todoDeleteEvent.subscribe((id: number) => {
        this.componentRefs = this.componentRefs
          .filter((item: ComponentRef<TodoItemComponent>) => item.instance.data.id !== id || item.destroy());
      })
    );

    this.componentRefs.push(componentRef);

    console.log('this.vc.get(0): ', this.vc.get(0));
    console.log('componentRefs: ', this.componentRefs);
  }

  changeComp0(): void {
    this.componentRefs[0].location.nativeElement.style.cursor = 'pointer';
    this.componentRefs[0].location.nativeElement.style.backgroundColor = 'tomato';
    this.componentRefs[0].location.nativeElement.onselectstart = () => false;
    this.componentRefs[0].location.nativeElement.onclick = () => alert('component1 clicked!');
  }

  changeComp1(): void {
    if (this.componentRefs.length <= 1) {
      return;
    }
    this.componentRefs[1].instance.changeDescription = `random number is: ${this.getRandomNumber()}`;
  }

  getRandomNumber(): number {
    return Math.floor(Math.random() * Math.floor(999));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
