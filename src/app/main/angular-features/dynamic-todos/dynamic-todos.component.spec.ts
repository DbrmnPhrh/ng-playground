import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicTodosComponent } from './dynamic-todos.component';

describe('CreateComponentComponent', () => {
  let component: DynamicTodosComponent;
  let fixture: ComponentFixture<DynamicTodosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicTodosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicTodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
