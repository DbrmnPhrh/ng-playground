import {Component, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoItemComponent {

  constructor(private cdr: ChangeDetectorRef) {
  }

  @Output() todoDeleteEvent: EventEmitter<number> = new EventEmitter<number>();
  data: ITodoItem;

  set changeDescription(newValue: string) {
    this.data.description = newValue;
    this.cdr.markForCheck();
  }

  deleteTodoItem(): void {
    this.todoDeleteEvent.emit(this.data.id);
  }
}

export interface ITodoItem {
  id: number;
  headerText: string;
  description: string;
}
