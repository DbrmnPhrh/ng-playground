import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-custom-tooltip',
  templateUrl: './custom-tooltip.component.html',
  styleUrls: ['./custom-tooltip.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomTooltipComponent {

  isTooltipVisible: boolean;
  tooltipStyle = {
    width: '300px',
    height: '100px',
    top: '0',
    left: '0',
  };

  constructor() {
  }

  public showTooltip = (val, event?): void => {
    this.isTooltipVisible = val;
    if (val) {
      this.calculateTooltipPosition(event);
    }
  }

  private calculateTooltipPosition = (event): void => {
    const x = event.clientX + 10;
    const y = event.clientY + 10;
    const width = parseInt(this.tooltipStyle.width, 0);
    const height = parseInt(this.tooltipStyle.height, 0);
    const adjustedX = x + width > window.innerWidth ? window.innerWidth - width : x;
    const adjustedY = y + height > window.innerHeight ? window.innerHeight - height : y;
    this.tooltipStyle = {
      ...this.tooltipStyle,
      top: `${adjustedY}px`,
      left: `${adjustedX}px`,
    };
  }
}
