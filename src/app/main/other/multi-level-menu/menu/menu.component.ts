import {ChangeDetectionStrategy, Component, OnInit, ViewContainerRef} from '@angular/core';
import {faAngleRight} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent implements OnInit {

  faAngleRight = faAngleRight;

  menuItems: IManualMenu = [
    {
      name: 'Item 1',
      fragment: 'item1',
    },
    {
      name: 'Item 2',
      fragment: 'item2',
      showChildren: true,
      children: [
        {
          name: 'Item 2.1',
          fragment: 'item2-1',
        },
        {
          name: 'Item 2.2',
          fragment: 'item2-2',
        },
        {
          name: 'Item 2.3',
          fragment: 'item2-3',
          showChildren: true,
          children: [
            {
              name: 'Item 2.3.1',
              fragment: 'item2-3-1',
            },
            {
              name: 'Item 2.3.2',
              fragment: 'item2-3-2',
            },
            {
              name: 'Item 2.3.3',
              fragment: 'item2-3-3',
            },
          ],
        },
      ]
    },
    {
      name: 'Item 3',
      fragment: 'item3',
    },
  ];

  constructor(private currentComponent: ViewContainerRef) { }

  ngOnInit(): void {
    this.currentComponent.element.nativeElement.onselectstart = () => false;
  }

  toggleChildren(index): void {
    this.menuItems[index].showChildren = !this.menuItems[index].showChildren;
  }

  toggleSecondLevelChildren(idx, secondIdx): void {
    this.menuItems[idx].children[secondIdx].showChildren = !this.menuItems[idx].children[secondIdx].showChildren;
  }
}

type IManualMenu = IManualMenuItems[];

interface IManualMenuItems {
  name: string;
  fragment: string;
  showChildren?: boolean;
  children?: IManualMenuItems[];
}
