import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-multi-level-menu',
  templateUrl: './multi-level-menu.component.html',
  styleUrls: ['./multi-level-menu.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiLevelMenuComponent {
}
