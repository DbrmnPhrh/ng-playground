import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-responsive-table',
  templateUrl: './responsive-table.component.html',
  styleUrls: ['./responsive-table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResponsiveTableComponent implements OnInit {

  /**
   * Responsive table design using only html css https://www.youtube.com/watch?v=ZtopjfXhUZI
   */

  constructor() { }

  ngOnInit(): void {
  }

}
