import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-style-features',
  templateUrl: './style-features.component.html',
  styleUrls: ['./style-features.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StyleFeaturesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
