import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {CustomCheckboxComponent} from './custom-checkbox/custom-checkbox.component';
import {MultiLevelMenuComponent} from './multi-level-menu/multi-level-menu.component';
import {SwitcherComponent} from './switcher/switcher.component';
import {NotFoundComponent} from './not-found/not-found.component';
import { ResponsiveTableComponent } from './responsive-table/responsive-table.component';
import { StyleFeaturesComponent } from './style-features/style-features.component';
import { NgSelectComponent } from './ng-select/ng-select.component';
import { CustomTooltipComponent } from './custom-tooltip/custom-tooltip.component';

const routes: Routes = [
  {path: '', pathMatch: 'prefix', redirectTo: 'custom-checkbox'},
  {path: 'custom-checkbox', component: CustomCheckboxComponent},
  {path: 'custom-tooltip', component: CustomTooltipComponent},
  {path: 'multi-level-menu', component: MultiLevelMenuComponent}, // при изменении url также исправить в main.component, иначе поедет вёрстка!
  {path: 'switcher', component: SwitcherComponent},
  {path: 'responsive-table', component: ResponsiveTableComponent},
  {path: 'not-found', component: NotFoundComponent},
  {path: 'style-features', component: StyleFeaturesComponent},
  {path: 'ng-select', component: NgSelectComponent},
];


@NgModule({
  declarations: [
    ResponsiveTableComponent,
    StyleFeaturesComponent,
    NgSelectComponent,
    CustomTooltipComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
  ]
})
export class OtherModule { }
