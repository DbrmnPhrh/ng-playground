import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found-child',
  templateUrl: './not-found-child.component.html',
  styleUrls: ['./not-found-child.component.sass'],
})
export class NotFoundChildComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
