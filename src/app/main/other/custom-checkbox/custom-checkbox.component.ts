import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-custom-checkbox',
  templateUrl: './custom-checkbox.component.html',
  styleUrls: ['./custom-checkbox.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomCheckboxComponent implements OnInit {

  isChecked: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
