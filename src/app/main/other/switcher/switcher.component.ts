import { Component, OnInit } from '@angular/core';
import {faAngleUp, faAngleRight, faAngleDown, faAngleLeft} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-switcher',
  templateUrl: './switcher.component.html',
  styleUrls: ['./switcher.component.sass']
})
export class SwitcherComponent implements OnInit {

  mode: 'UP' | 'RIGHT' | 'DOWN' | 'LEFT' = 'UP';

  faAngleUp = faAngleUp;
  faAngleRight = faAngleRight;
  faAngleDown = faAngleDown;
  faAngleLeft = faAngleLeft;

  constructor() { }

  ngOnInit(): void {
  }

  toggleMode(): void {
    switch (this.mode) {
      case 'UP':
        this.mode = 'RIGHT';
        break;
      case 'RIGHT':
        this.mode = 'DOWN';
        break;
      case 'DOWN':
        this.mode = 'LEFT';
        break;
      case 'LEFT':
        this.mode = 'UP';
        break;
    }
  }
}
