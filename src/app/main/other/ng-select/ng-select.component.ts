import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {NgSelectConfig} from '@ng-select/ng-select';
import {MainService} from '../../main.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-ng-select',
  template: `
    <div class="container">
      <ng-select
        [items]="items"
        [virtualScroll]="true"
        [bindLabel]="'name'"
        [bindValue]="'id'"
        (change)="onChange($event)"
        (search)="filterItems($event)"
      >
        <ng-option *ngFor="let item of filteredItems" [value]="item.id">{{item.name}}</ng-option>
      </ng-select>
      <p>{{selectedValue}}</p>
    </div>
  `,
  styleUrls: ['./ng-select.component.sass'],
  changeDetection: ChangeDetectionStrategy.Default // todo
})
export class NgSelectComponent implements OnInit {

  /**
   * https://github.com/ng-select/ng-select/
   * https://github.com/rd003/angular-23/blob/master/src/app/components/autocomplete-demo/autocomplete-demo.component.html
   */

  items: {id: number; name: string}[] = []; // Ваш массив данных
  filteredItems = []; // Массив данных после фильтрации
  selectedValue: string;

  constructor(
    private config: NgSelectConfig,
    private mainService: MainService,
  ) {
    this.config.notFoundText = 'Custom not found text';
    this.config.appendTo = 'body';
    this.config.placeholder = 'some placeholder';
    this.config.bindValue = 'value';
  }

  ngOnInit(): void {
    this.filteredItems = this.items;
    this.mainService.getDataProvider('https://fakestoreapi.com/products/')
      .pipe(map((data: any[]) => data.map(m => ({
        id: m.id,
        name: m.title,
      }))))
      .subscribe((data: {id: number; name: string}[]) => this.items = data);
  }

  filterItems(event): void {
    const query = event.term.toLowerCase();
    this.filteredItems = this.items.filter((item) => item.name.toLowerCase().includes(query));
  }

  onChange(item): void {
    if (item) {
      this.selectedValue = item.name;
    }
  }

}
