import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomStyleDirective} from './directives/custom-style.directive';
import {TimePipe} from './pipes/time';
import {FormsModule} from '@angular/forms';
import {NumberFormatDirective} from './directives/number-format.directive';
import {NgSelectModule} from '@ng-select/ng-select';
import { ClickOutsideDirective } from './directives/click-outside.directive';

const exports = [
  NgSelectModule,
  FormsModule,
];

@NgModule({
  declarations: [
    CustomStyleDirective,
    TimePipe,
    NumberFormatDirective,
    ClickOutsideDirective,
  ],
  imports: [
    CommonModule,
    exports,
  ],
  exports: [
    CustomStyleDirective,
    TimePipe,
    NumberFormatDirective,
    ClickOutsideDirective,
    exports,
  ]
})
export class SharedModule { }
