import {Directive, ElementRef, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appCustomStyle]'
})
export class CustomStyleDirective {

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
  ) { }

  @Input() set appCustomStyle(condition: boolean) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'color', condition ? 'magenta' : 'unset');
    this.renderer.setStyle(this.elementRef.nativeElement, 'fontWeight', condition ? 'bold' : 'unset');
    this.renderer.setStyle(this.elementRef.nativeElement, 'fontStyle', condition ? 'italic' : 'unset');
    this.renderer.setStyle(this.elementRef.nativeElement, 'fontSize', condition ? '24px' : 'unset');
    this.renderer.setStyle(this.elementRef.nativeElement, 'border', condition ? '2px dashed red' : 'unset');
  }
}
