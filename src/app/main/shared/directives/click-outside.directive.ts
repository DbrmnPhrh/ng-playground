import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appClickOutside]'
})
export class ClickOutsideDirective {
  @Output() appClickOutside = new EventEmitter();
  enableClicks: boolean;

  constructor(private elementRef: ElementRef) {
  }

  @HostListener('document:click', ['$event.target'])
  public onClick(target): void {
    const clickedInside = this.elementRef.nativeElement.contains(target);
    /**
     * setTimeout() исправляет проблему ложного срабатывания onClick когда директива вешается на элемент с *ngIf,
     * иначе в момент добавления элемента в DOM дерево onClick срабатывает сразу же
     */
    setTimeout(() => this.enableClicks = true);
    if (!clickedInside && this.enableClicks) {
      this.appClickOutside.emit();
    }
  }
}
