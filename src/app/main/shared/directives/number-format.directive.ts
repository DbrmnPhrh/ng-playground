import {Directive, ElementRef, HostBinding, HostListener, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appNumberFormat]',
})

export class NumberFormatDirective implements OnInit {
  @Input() appNumberFormat: NumberFormatProps;
  @HostBinding('class.hide-spinners') get hideSpinners(): boolean { return this.appNumberFormat.hideSpinners; }
  @HostListener('keydown', ['$event']) onKeyDown(e: KeyboardEvent): void {
    if (this.appNumberFormat.integerOnly && (e.key === '.' || e.key === ',')) {
      e.preventDefault();
    }

    if (this.appNumberFormat.minValue === 0 && e.code === 'Minus') {
      e.preventDefault();
    }
  }

  @HostListener('input') valueChange(): void {

    const value = this.element.nativeElement.value;

    this.element.nativeElement.onblur = () => {
      if (this.element.nativeElement.value === null ||
        this.element.nativeElement.value === undefined ||
        this.element.nativeElement.value === '') {
        this.element.nativeElement.value = null;
        return;
      }

      if (typeof this.appNumberFormat.minValue === 'number' && (value < this.appNumberFormat.minValue)) {
        this.element.nativeElement.value = this.appNumberFormat.minValue;
      }

      if (typeof this.appNumberFormat.maxValue === 'number' && (value > this.appNumberFormat.maxValue)) {
        this.element.nativeElement.value = this.appNumberFormat.maxValue;
      }

      if (this.appNumberFormat.integerOnly && !Number.isInteger(value)) {
        this.element.nativeElement.value = Math.trunc(this.element.nativeElement.value);
      }
    };
  }

  constructor(private element: ElementRef) {
  }

  ngOnInit(): void {
    if (!this.appNumberFormat) {
      return;
    }

    this.element.nativeElement.min = this.appNumberFormat.minValue;
    this.element.nativeElement.max = this.appNumberFormat.maxValue;
  }
}

interface NumberFormatProps {
  integerOnly?: boolean;   /** Только целочисленные значения */
  hideSpinners?: boolean;  /** Скрыть/показать кнопки инкремента/декремента */
  minValue?: number;       /** Минимальное значение */
  maxValue?: number;       /** Максимальное значение */
}
