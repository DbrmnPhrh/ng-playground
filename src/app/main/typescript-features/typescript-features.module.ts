import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {TsFeaturesComponent} from './ts-features/ts-features.component';


const routes: Routes = [
  {path: '', pathMatch: 'prefix', redirectTo: 'type-features'},
  {path: 'type-features', component: TsFeaturesComponent},
];

@NgModule({
  declarations: [
    TsFeaturesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
})

export class TypescriptFeaturesModule { }
