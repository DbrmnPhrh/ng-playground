import { Component } from '@angular/core';

@Component({
  selector: 'app-ts-features',
  templateUrl: './ts-features.component.html',
})
export class TsFeaturesComponent {

  // использование & в интерфейсах
  public readonly abc: interfaces.A & interfaces.B & interfaces.C = {
    a: 'string1',
    b: 'string2',
    c: 'string3',
  };

  // приватная переменная, не видна в шаблоне
  #someVar = 'some text';

  // строковый тип с определённым форматом
  formatVar: `${number}-${number}-${number}` = '1-2-3';
}

namespace interfaces {
  export interface A {
    a: string;
  }

  export interface B {
    b: string;
  }

  export interface C {
    c: string;
  }
}
