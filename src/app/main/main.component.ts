import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-main',
  template: `<router-outlet></router-outlet>`,
  styles: [`
    :host {
      flex: 5;
      width: 70vw;
      padding: 10px 10px 10px 20px;
    }`
  ],
})
export class MainComponent implements OnInit, OnDestroy {

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

}
