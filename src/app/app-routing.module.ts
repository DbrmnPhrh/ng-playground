import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AngularFeaturesModule} from './main/angular-features/angular-features.module';
import {RxjsModule} from './main/rxjs/rxjs.module';
import {TypescriptFeaturesModule} from './main/typescript-features/typescript-features.module';
import {OtherModule} from './main/other/other.module';

const routes: Routes = [
  {
    path: 'angular-features',
    loadChildren: () => import('./main/angular-features/angular-features.module').then(m => m.AngularFeaturesModule),
    data: {
      moduleName: 'Angular features',
    },
  },
  {
    path: 'rxjs',
    loadChildren: () => import('./main/rxjs/rxjs.module').then(m => m.RxjsModule),
    data: {
      moduleName: 'RxJS features',
    },
  },
  {
    path: 'typescript',
    loadChildren: () => import('./main/typescript-features/typescript-features.module').then(m => m.TypescriptFeaturesModule),
    data: {
      moduleName: 'Typescript features',
    },
  },
  {
    path: 'other',
    loadChildren: () => import('./main/other/other.module').then(m => m.OtherModule),
    data: {
      moduleName: 'Other features',
    },
  },
  {path: '**', redirectTo: 'not-found'},
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule,
    RouterModule.forRoot(routes),
    AngularFeaturesModule,
    RxjsModule,
    TypescriptFeaturesModule,
    OtherModule,
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule { }
