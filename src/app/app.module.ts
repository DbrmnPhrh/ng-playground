import {BrowserModule} from '@angular/platform-browser';
import {InjectionToken, LOCALE_ID, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {TemplateTestComponent} from './main/angular-features/template-test/template-test.component';
import {CustomCheckboxComponent} from './main/other/custom-checkbox/custom-checkbox.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {MainComponent} from './main/main.component';
import {NotFoundComponent} from './main/other/not-found/not-found.component';
import {NotFoundChildComponent} from './main/other/not-found/not-found-child/not-found-child.component';
import {NgContentChildComponent} from './main/angular-features/ng-content/ng-content-child/ng-content-child.component';
import {NgContentComponent} from './main/angular-features/ng-content/ng-content.component';
import {MultiLevelMenuComponent} from './main/other/multi-level-menu/multi-level-menu.component';
import {MenuComponent} from './main/other/multi-level-menu/menu/menu.component';
import {ContentComponent} from './main/other/multi-level-menu/content/content.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {UserByIdComponent} from './main/angular-features/user-by-id/user-by-id.component';
import {SwitcherComponent} from './main/other/switcher/switcher.component';
import {UsersFromResolver} from './main/angular-features/user-by-id-resolver/users-from-resolver.component';
import {AppRoutingModule} from './app-routing.module';
import {CommonModule} from '@angular/common';
import {TokensTest2Service} from './main/angular-features/tokens-test/tokens-test2.service';
import {TokensTest3Service} from './main/angular-features/tokens-test/tokens-test3.service';

export const DEFAULT_SETTINGS = new InjectionToken<string>(
  'settings',
  {
    providedIn: 'root',
    factory: () => {
      return 'factory test some string';
    },
  }
);

const defaultSettings: IDefaultSettings = {
  someProperty: true,
  someText: 'default string'
};

@NgModule({
  declarations: [
    AppComponent,
    TemplateTestComponent,
    CustomCheckboxComponent,
    SidebarComponent,
    MainComponent,
    NotFoundComponent,
    NotFoundChildComponent,
    NgContentComponent,
    NgContentChildComponent,
    MultiLevelMenuComponent,
    MenuComponent,
    ContentComponent,
    UserByIdComponent,
    SwitcherComponent,
    UsersFromResolver,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    FontAwesomeModule,
    AppRoutingModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'ru' },
    /** @see TokensTest1Service - 1 вариант регистрации сервиса через @Injectable({ providedIn: 'root' }) */
    TokensTest2Service, // 2 вариант регистрации сервиса
    { provide: TokensTest2Service }, // 3 вариант регистрации сервиса
    { provide: TokensTest3Service, useClass: TokensTest2Service }, // переопределение сервиса (это будет отдельный инстанс TokensTest2Service)
    /** Какие-нибудь дефолтные настройки, используются в:
     * @see TokensTestComponent
     * @see ViewChildParentComponent
     */
    { provide: DEFAULT_SETTINGS, useValue: defaultSettings },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export interface IDefaultSettings {
  someProperty: boolean;
  someText: string;
}
